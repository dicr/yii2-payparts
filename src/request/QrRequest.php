<?php
/*
 * @copyright 2019-2022 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license proprietary
 * @version 05.01.22 03:11:24
 */

declare(strict_types = 1);
namespace dicr\payparts\request;

use dicr\json\JsonEntity;
use dicr\payparts\PayParts;
use dicr\payparts\PayPartsModule;
use dicr\validate\ValidateException;
use Yii;
use yii\base\Exception;
use yii\httpclient\Client;

/**
 * Генерация QR-кода для подтверждения сделки.
 *
 * @link https://bw.gitbooks.io/api-oc/content/qr_code.html
 */
class QrRequest extends JsonEntity implements PayParts
{
    /** @var ?string токен, полученный при создании платежа */
    public ?string $token = null;

    /** @var ?mixed размер картинки */
    public mixed $size = null;

    /** @var ?float окончательная сумма покупки */
    public ?float $amount = null;

    /**
     * @var ?string тип рассрочки
     * @see PayParts::MERCHANT_TYPES
     */
    public ?string $type = null;

    /**
     * QrRequest constructor.
     */
    public function __construct(
        private PayPartsModule $module,
        array $config = []
    ) {
        parent::__construct($config);
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        // не учитываем parent::orderId
        return [
            ['token', 'trim'],
            ['token', 'required'],

            ['size', 'default'],

            ['amount', 'required'],
            ['amount', 'number', 'min' => 0.01],
            ['amount', 'filter', 'filter' => static fn($val): float => round((float)$val, 2)],

            ['type', 'default'],
            ['type', 'in', 'range' => self::MERCHANT_TYPES]
        ];
    }

    /**
     * Отправляет запрос.
     *
     * @return string QR-код
     * @throws Exception
     */
    public function send(): string
    {
        // валидация полей
        if (! $this->validate()) {
            throw new ValidateException($this);
        }

        // фильтруем данные
        $data = array_filter(
            $this->getJson(),
            static fn($val): bool => $val !== null && $val !== '' && $val !== []
        );

        // запрос
        $request = $this->module->httpClient->get(self::QR_URL, $data, [
            'Accept' => 'application/json',
            'Accept-Encoding' => 'UTF-8',
        ]);

        // получаем ответ
        Yii::debug('Запрос: ' . $request->toString(), __METHOD__);
        $response = $request->send();
        Yii::debug('Ответ: ' . $response->toString(), __METHOD__);

        if (! $response->isOk) {
            throw new Exception('HTTP error: ' . $response->statusCode);
        }

        $response->format = Client::FORMAT_JSON;

        // проверяем состояние
        if (($response->data['state'] ?? '') !== self::STATE_SUCCESS) {
            throw new Exception('Ошибка запроса: ' . ($response->data['message'] ?? $response->content));
        }

        // проверяем наличие результата
        $qr = (string)($response->data['qr'] ?? '');
        if ($qr === '') {
            throw new Exception('Не получен QR-код: ' . $response->content);
        }

        // возвращаем QR-код
        return $qr;
    }
}
