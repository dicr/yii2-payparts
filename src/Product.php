<?php
/*
 * @copyright 2019-2022 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license proprietary
 * @version 05.01.22 03:06:46
 */

declare(strict_types = 1);
namespace dicr\payparts;

use dicr\json\JsonEntity;

/**
 * Данные товара.
 *
 * @property-read float $sum сумма
 */
class Product extends JsonEntity
{
    public ?string $name = null;

    public ?int $count = null;

    public ?float $price = null;

    /**
     * @inheritDoc
     */
    public function attributeFields(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            ['name', 'trim'],
            ['name', 'required'],
            ['name', 'string', 'max' => 128],

            ['count', 'required'],
            ['count', 'integer', 'min' => 1],
            ['count', 'filter', 'filter' => 'intval'],

            ['price', 'required'],
            ['price', 'number', 'min' => 0.01],
            ['price', 'filter', 'filter' => static fn($val): float => round((float)$val, 2)]
        ];
    }

    /**
     * Сумма.
     */
    public function getSum(): float
    {
        return $this->count * round((float)$this->price, 2);
    }
}
